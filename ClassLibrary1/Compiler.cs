﻿using ClassLibrary2;

namespace ClassLibrary1
{
    public class Compiler
    {
        private Jint.Engine compiler;

        public Compiler()
        {
            compiler = new Jint.Engine();
        }

        public object Execute(string code, Poco poco, int value)
        {
            return compiler
                .SetValue("poco", poco)
                .SetValue("value", value)
                .Execute(code).GetCompletionValue().ToObject();
        }
    }
}
